# EKS-cluster-setup 



 Successfully Set Up an EKS Cluster with Terraform! 

As a DevOps enthusiast, I recently completed an exciting project where I set up an Amazon EKS (Elastic Kubernetes Service) cluster to run our Kubernetes applications. 

#Tools and Technologies Used:

Terraform: For Infrastructure as Code (IaC) to create and manage the EKS cluster.

AWS CLI: For interacting with AWS services.

kubectl: To manage Kubernetes resources.

AWS IAM Authenticator: For Kubernetes authentication using AWS IAM.


 #Verification:

Verified the setup by listing the Kubernetes nodes using kubectl.
Ensured secure and efficient interaction with the EKS cluster using IAM roles and policies.
 Key Takeaways:

Automation: Using Terraform, we automated the entire infrastructure setup, ensuring consistency and repeatability.

Security: Leveraged AWS IAM for secure authentication and authorization.

Scalability: The EKS cluster is now ready to scale our applications seamlessly.

This project was a great example of how modern DevOps practices can streamline infrastructure management and improve deployment efficiency. Looking forward to deploying more Kubernetes applications on our new EKS cluster!

#Contact:

For any queries or support, please contact at [shiivamvighne@gmail.com]

